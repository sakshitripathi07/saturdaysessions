# CACHING APPROACHES

**Caching** is a technique to speed up data reading. Instead of reading the data directly from its source, which could be a database or another remote system, the data is read directly from a cache on the computer that needs the data.

A _cache hit_ occurs when the requested data can be found in a cache, while a _cache miss_ occurs when it cannot be found in a cache. Cache hits are served by reading data from the cache, which is faster than recomputing a result or reading from a slower data store. The more requests that can be served from the cache, the faster the system performs.
To be cost-effective and to enable efficient use of data, caches must be relatively small.

## APPROACHES

### DATA CACHING / DATABASE CACHING

Database caching allows you to dramatically increase throughput and lower the data retrieval latency associated with backend databases, which as a result, improves the overall performance of your applications. The cache acts as an adjacent _data access layer_ to your database that your applications can utilize in order to improve performance. A database cache layer can be applied in front of any type of database, including **relational** and **NoSQL databases**. Common techniques used to load data into you’re a cache include _lazy loading_ and _write-through methods_.

### WEB CACHING

While delivering web content to viewers, a lot of latency is involved while retrieving images, Html documents, videos, etc. It can be greatly reduced by caching these artifacts and eliminating disk reads and server load. Various web caching techniques can be employed both on the server and on the client-side. **Server-side** web caching typically involves utilizing a _web proxy_ which retains web responses from the web servers it sits in front of, effectively reducing their load and latency. **Client-side** web caching can include browser-based caching which retains a cached version of the previously visited web content.

### APPLICATION CACHING

An _Application Program Interface_ generally is a RESTful web service that can be accessed over HTTP and exposes resources that allow the user to interact with the application.
It is important to consider the expected load on API, authorization to it, the effect of version change on the customer and ease of use.
_Serving a cached result_ of the API will deliver the most optimal and cost-effective response.
By caching API response, you eliminate pressure to infrastructure including application servers and databases which results in faster response time and deliver a more performant API.

### DISTRIBUTED CACHING

_Distributed caching_ comprises of a cluster of cache memories which may be distributed over a large geographical area. With the help of distributed caching we can serve the user without fail, regardless of the user's geographical location. It uses a wide network of cache in this type of caching. This type of caching can cache a large amount of data and serve when the user requests for it.

### INTEGRATED CACHING

An _integrated cache_ is an in-memory layer that automatically caches frequently accessed data from the origin database. Most commonly, the underlying database will utilize the cache to serve the response to the inbound database request given the data is resident in the cache. This increases the performance of the database by lowering the request latency and reducing CPU and memory utilization on the database engine. An important characteristic of an integrated cache is that the data cached is consistent with the data stored on disk by the database engine.

### CONTENT DELIVERY NETWORK(CDN)

A CDN provides the ability to utilize a global network of edge locations to deliver a cached copy of web content such as videos, webpages, images to customers. To reduce response time, the CDN utilizes the nearest edge location to the customer or originating request location in order to reduce the response time. Throughput is increased given that the web assets are delivered from the cache. For dynamic data, many CDNs can be configured to retrieve data from the origin servers.

### DOMAIN NAME SYSTEM CACHING

A domain request made queries DNS cache servers in order to resolve the IP address associated with the domain name. DNS caching can occur on many levels including on the OS, via ISPs and DNS servers.

## REFERENCES

- <https://en.wikipedia.org/wiki/Cache_(computing)>

- <https://aws.amazon.com/caching>
